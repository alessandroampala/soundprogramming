# SoundProgramming
This is an **audio library** based on FMOD Core API.
This is the final project for the Sound Programming module of the Master Game Development.

## Project structure
The solution contains 2 projects:
- `AudioLibrary`: This is the audio library itself. When compiled produces a .lib file.
- `AudioClient`: This example project illustrates how the AudioLibrary works and how to use it.

## How to use it
From Visual Studio, building the `AudioClient` will also automatically build the library. There's nothing else to do!

## Important!
In order to compile, **this project requires you to have the FMOD Core API installed at the default installation path** (C:\Program Files (x86)\FMOD SoundSystem\FMOD Studio API Windows\api\core). This is because this library is based on the FMOD Core API `.h`, `.lib` and `.dll`.
