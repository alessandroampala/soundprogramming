#include "AudioLibrary.h"

void AudioLibrary::Initialize(int maxChannels)
{
	FMOD_RESULT result;
	result = FMOD::System_Create(&system);
	ERRCHECK(result);

	result = system->init(32, FMOD_INIT_NORMAL, nullptr);
	ERRCHECK(result);
}

AudioLibrary::Sound AudioLibrary::Load(const char* fileName, LoadMode loadMode, SoundMode soundMode /* = SoundMode::DEFAULT*/)
{
	FMOD_RESULT result;
	Sound sound;

	result = system->createSound(fileName, loadMode | soundMode, 0, &sound);
	ERRCHECK(result);
	return sound;
}

void AudioLibrary::Play(AudioLibrary::Sound sound, AudioLibrary::Channel* channel)
{
	FMOD_RESULT result;
	result = system->playSound(sound, 0, false, channel);
	ERRCHECK(result);
}

void AudioLibrary::TogglePause(AudioLibrary::Channel channel)
{
	FMOD_RESULT result;
	bool paused;
	result = channel->getPaused(&paused);
	ERRCHECK(result);
	result = channel->setPaused(!paused);
	ERRCHECK(result);
}

void AudioLibrary::Stop(AudioLibrary::Channel channel)
{
	FMOD_RESULT result;
	result = channel->stop();
	ERRCHECK(result);
}

void AudioLibrary::SetPan(AudioLibrary::Channel channel, float pan)
{
	FMOD_RESULT result;
	result = channel->setPan(pan);
	ERRCHECK(result);
}

void AudioLibrary::SetVolume(Channel channel, float volume)
{
	channel->setVolume(volume);
}

int AudioLibrary::GetChannelsPlaying()
{
	int channels;
	system->getChannelsPlaying(&channels, NULL);
	return channels;
}

bool AudioLibrary::GetPaused(Channel channel)
{
	FMOD_RESULT result;
	bool paused;
	result = channel->getPaused(&paused);
	if ((result != FMOD_OK) && (result != FMOD_ERR_INVALID_HANDLE) && (result != FMOD_ERR_CHANNEL_STOLEN))
	{
		ERRCHECK(result);
	}
	return paused;
}

void AudioLibrary::Update()
{
	FMOD_RESULT result;
	result = system->update();
	ERRCHECK(result);
}

AudioLibrary::Sound AudioLibrary::GetCurrentSound(Channel ch)
{
	Sound sound = nullptr;
	ch->getCurrentSound(&sound);
	return sound;
}

unsigned int AudioLibrary::GetLength(Sound sound)
{
	FMOD_RESULT result;
	unsigned int length;
	result = sound->getLength(&length, FMOD_TIMEUNIT_MS);
	if ((result != FMOD_OK) && (result != FMOD_ERR_INVALID_HANDLE) && (result != FMOD_ERR_CHANNEL_STOLEN))
	{
		ERRCHECK(result);
	}
	return length;
}

bool AudioLibrary::IsPlaying(Channel channel)
{
	FMOD_RESULT result;
	bool playing;
	result = channel->isPlaying(&playing);
	if ((result != FMOD_OK) && (result != FMOD_ERR_INVALID_HANDLE) && (result != FMOD_ERR_CHANNEL_STOLEN))
	{
		ERRCHECK(result);
	}
	return playing;
}

unsigned int AudioLibrary::GetPosition(Channel channel)
{
	FMOD_RESULT result;
	unsigned int ms;
	result = channel->getPosition(&ms, FMOD_TIMEUNIT_MS);
	if ((result != FMOD_OK) && (result != FMOD_ERR_INVALID_HANDLE) && (result != FMOD_ERR_CHANNEL_STOLEN))
	{
		ERRCHECK(result);
	}
	return ms;
}
