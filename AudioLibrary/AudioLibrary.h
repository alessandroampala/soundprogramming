#pragma once

#include "fmod.hpp"
#include "common.h"


enum LoadMode
{
	STATIC = FMOD_DEFAULT,
	STREAMING = FMOD_CREATESTREAM,
};

enum SoundMode
{
	/*
	* Default mode, doesn't disable loop points.
	* To make sure the sound is one-shot, use ONE_SHOT instead.
	*/
	DEFAULT = FMOD_DEFAULT,
	ONE_SHOT = FMOD_LOOP_OFF,
	LOOP = FMOD_LOOP_NORMAL,
};

class AudioLibrary
{
public:
	using Sound = FMOD::Sound*;
	using Channel = FMOD::Channel*;

	// Initialize the system. Must be called before anything else.
	void Initialize(int maxChannels);

	// Load a sound from file and set properties
	Sound Load(const char* fileName, LoadMode loadMode, SoundMode soundMode = SoundMode::DEFAULT);

	// Play a sound on a channel
	void Play(AudioLibrary::Sound sound, AudioLibrary::Channel* channel);

	// Toggle Play/Pause on a channel
	void TogglePause(AudioLibrary::Channel channel);

	// Stop sound on a channel
	void Stop(AudioLibrary::Channel channel);

	// Set pan on a channel
	// -1 represents full left, 0 represents center and 1 represents full right
	void SetPan(AudioLibrary::Channel channel, float pan /*[0,1]*/);

	// Set volume level
	// 0 = silent, 1 = full. Negative level inverts the signal. Values larger than 1 amplify the signal.
	void SetVolume(Channel ch, float volume /*[0,1]*/);

	//Get number of playing channels
	int GetChannelsPlaying();

	//Get sound length in ms
	unsigned int GetLength(Sound sound);

	//Get sound progress in ms
	unsigned int GetPosition(Channel channel);

	bool IsPlaying(Channel channel);
	bool GetPaused(Channel channel);

	//Get sound in channel
	Sound GetCurrentSound(Channel channel);
	
	void Update(); /* IMPORTANT! CALL THIS ONCE PER FRAME! */

private:
	FMOD::System* system;
};
