#include "AudioLibrary.h"
#include "common.h"

int FMOD_Main()
{
    void* extradriverdata = 0;
    Common_Init(&extradriverdata);

    AudioLibrary::Sound sound1;
    AudioLibrary::Sound sound2;
    AudioLibrary::Sound sound3;

    AudioLibrary system;
    AudioLibrary::Channel c = 0;

    //initialize system
    system.Initialize(32);

    //load sounds
    sound1 = system.Load(Common_MediaPath("drumloop.wav"), STATIC, ONE_SHOT);
    sound2 = system.Load(Common_MediaPath("jaguar.wav"), STATIC);
    sound3 = system.Load(Common_MediaPath("swish.wav"), STREAMING, LOOP);

    float volume = 1;
    float pan = 0;

    /*
        Main loop
    */
    do
    {
        Common_Update();

        //Get inputs

        if (Common_BtnPress(BTN_ACTION1))
        {
            system.Play(sound1, &c);
        }

        if (Common_BtnPress(BTN_ACTION2))
        {
            system.Play(sound2, &c);
        }

		if (Common_BtnPress(BTN_ACTION3))
		{
			system.Play(sound3, &c);
		}

        if (Common_BtnPress(BTN_MORE) && system.IsPlaying(c))
        {
            system.TogglePause(c);
        }

        if (Common_BtnPress(BTN_ACTION4) && system.IsPlaying(c))
        {
            system.Stop(c);
        }

		if (Common_BtnPress(BTN_RIGHT))
		{
            pan += 0.1f;
            if (pan < -1) pan = -1;
            else if (pan > 1) pan = 1;
		}

		if (Common_BtnPress(BTN_LEFT))
		{
			pan -= 0.1f;
			if (pan < -1) pan = -1;
			else if (pan > 1) pan = 1;
		}

        if (Common_BtnPress(BTN_UP))
        {
			volume += 0.1f;
			if (volume < 0) volume = 0;
			else if (volume > 1) volume = 1;
        }

		if (Common_BtnPress(BTN_DOWN))
		{
			volume -= 0.1f;
			if (volume < 0) volume = 0;
			else if (volume > 1) volume = 1;
		}

        system.SetVolume(c, volume);
        if(system.IsPlaying(c))
            system.SetPan(c, pan);

        system.Update();

        {
            unsigned int ms = 0;
            unsigned int lenms = 0;
            bool         playing = 0;
            bool         paused = 0;
            int          channelsplaying = 0;

			if (c)
			{
				AudioLibrary::Sound currentsound = 0;
                playing = system.IsPlaying(c);
                paused = system.GetPaused(c);
                ms = system.GetPosition(c);
				currentsound = system.GetCurrentSound(c);
				if (currentsound)
				{
                    lenms = system.GetLength(currentsound);
				}
			}

            channelsplaying = system.GetChannelsPlaying();

            //create progress bar
			char progressBar[43] = "|----------------------------------------|";
            if (system.IsPlaying(c))
            {
				int index = ms * 40 / lenms;
                progressBar[index + 1] = 'o';
            }

            Common_Draw("==================================================");
            Common_Draw("AudioLibrary Example.");
            Common_Draw("==================================================");
            Common_Draw("");
            Common_Draw("Press %s to play a mono sound (drumloop)", Common_BtnStr(BTN_ACTION1));
            Common_Draw("Press %s to play a mono sound (jaguar)", Common_BtnStr(BTN_ACTION2));
            Common_Draw("Press %s to play a stereo loop sound (swish)", Common_BtnStr(BTN_ACTION3));
            Common_Draw("");
            Common_Draw("Press %s to toggle Play/Pause", Common_BtnStr(BTN_MORE));
            Common_Draw("Press %s to Stop", Common_BtnStr(BTN_ACTION4));
            Common_Draw("Press %s and %s to adjust the Volume", Common_BtnStr(BTN_UP), Common_BtnStr(BTN_DOWN));
            Common_Draw("Press %s and %s to adjust the Pan", Common_BtnStr(BTN_LEFT), Common_BtnStr(BTN_RIGHT));
            Common_Draw("Press %s to quit", Common_BtnStr(BTN_QUIT));
            Common_Draw("");
            Common_Draw("Volume: %.1f", volume);
            Common_Draw("Pan: %.1f", pan);
			Common_Draw("");
            Common_Draw("Time %02d:%02d:%02d/%02d:%02d:%02d : %s", ms / 1000 / 60, ms / 1000 % 60, ms / 10 % 100, lenms / 1000 / 60, lenms / 1000 % 60, lenms / 10 % 100, paused ? "Paused " : playing ? "Playing" : "Stopped");
            Common_Draw("%s", progressBar);
            Common_Draw("Channels Playing %d", channelsplaying);
        }

        Common_Sleep(50);
    } while (!Common_BtnPress(BTN_QUIT));

    /*
        Shut down
    */

    Common_Close();

    return 0;
}
